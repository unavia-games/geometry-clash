﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GunController))]
public class CrosshairController : ExtendedMonoBehaviour
{
    public enum RotationType
    {
        ALWAYS,
        NEVER,
    }

    public enum TargetingState
    {
        DISABLED,
        OBSTRUCTED,
        TARGETING
    }


    /// <summary>
    /// Crosshairs image
    /// </summary>
    public GameObject Crosshairs;
    /// <summary>
    /// Middle dot in crosshairs
    /// </summary>
    public GameObject Dot;
    /// <summary>
    /// Layers that will block crosshair line-of-sight
    /// </summary>
    public LayerMask ObstructionLayerMask;
    [SerializeField]
    [ReadOnly]
    private TargetingState targetingState;

    [Header("Rotation")]
    /// <summary>
    /// Whether crosshairs should rotate normally
    /// </summary>
    public RotationType Rotate = RotationType.ALWAYS;
    /// <summary>
    /// Number of rotations per second
    /// </summary>
    [Range(0.1f, 1f)]
    public float RotationSpeed = 0.25f;

    [Header("Color")]
    /// <summary>
    /// Original crosshairs color
    /// </summary>
    public Color OriginalColor = Color.black;
    /// <summary>
    /// Original crosshairs dot color
    /// </summary>
    public Color OriginalDotColor = Color.white;
    /// <summary>
    /// Original crosshairs transparency
    /// </summary>
    public float OriginalAlpha = 1.0f;

    private GunController gunController;
    private Transform gunTransform;
    private SpriteRenderer sprite;
    private Color imageColor;
    private float imageAlpha;


    private void Start()
    {
        // Weapon position determines whether target can be reached
        gunController = GetComponent<GunController>();
        gunTransform = gunController.GunTransform;

        targetingState = TargetingState.DISABLED;

        sprite = Crosshairs.GetComponent<SpriteRenderer>();
        imageColor = OriginalColor;
        imageAlpha = OriginalAlpha;
    }

    void Update()
    {
        // Rotate crosshairs continually
        if (Rotate == RotationType.ALWAYS)
        {
            float rotation = RotationSpeed * Time.deltaTime * 360;
            Crosshairs.transform.Rotate(0, 0, rotation);
        }

        // Crosshairs are positioned on the mouse
        Vector3? targetPosition = CameraUtils.GetMousePointOnPlane(CameraManager.Instance.MainCamera, gunTransform.position.y);
        if (targetPosition.HasValue)
        {
            Crosshairs.transform.position = targetPosition.Value;
        }

        // Use color to indicate crosshair targeting status
        targetingState = GetCrosshairState(targetPosition);
        sprite.color = Color.Lerp(sprite.color, GetCrosshairColor(targetingState), 10f * Time.deltaTime);
    }


    /// <summary>
    /// Indicate crosshair targeting status with a status color
    /// </summary>
    /// <param name="targetPosition">Crosshair target position</param>
    /// <returns>Color for current targeting status</returns>
    private Color GetCrosshairColor(TargetingState targetingState)
    {
        switch(targetingState) {
            case TargetingState.DISABLED:
                Color noGunColor = Color.grey;
                noGunColor.a = 0.75f;
                return noGunColor;
            case TargetingState.OBSTRUCTED:
                Color obstructedColor = Color.red;
                obstructedColor.a = 0.5f;
                return obstructedColor;
            case TargetingState.TARGETING:
            default:
                Color original = OriginalColor;
                original.a = OriginalAlpha;
                return original;
        }
    }

    /// <summary>
    /// Indicate crosshair targeting status
    /// </summary>
    /// <param name="targetPosition"></param>
    /// <returns>Current targeting status</returns>
    private TargetingState GetCrosshairState(Vector3? targetPosition)
    {
        // Having no gun equipped, reloading, or empty should disable crosshairs
        if (!gunController.HasGunEquipped || gunController.EquippedGun.IsReloading || gunController.EquippedGun.IsClipEmpty) {
            return TargetingState.DISABLED;
        }

        // Having no targeted position should also disable crosshairs
        if (!targetPosition.HasValue)
        {
            return TargetingState.DISABLED;
        }

        // Range to target can be limited by gun's maximum range
        float maxGunRange = gunController.EquippedGun.ProjectileData.HasMaxRange
            ? gunController.EquippedGun.ProjectileData.MaxRange
            : 0f;
        float distanceToTarget = Vector3.Distance(targetPosition.Value, gunTransform.position);
        float obstacleCheckRayLength = Mathf.Min(maxGunRange, distanceToTarget);
        Vector3 directionToTarget = targetPosition.Value - gunController.EquippedGun.FiringTransform.position;

        if (GameManager.Instance.DebugMode)
        {
            Debug.DrawRay(gunTransform.position, directionToTarget, Color.cyan);
        }

        // Casting ray determines if view of target is obstructed by environment
        Ray ray = new Ray(gunTransform.position, directionToTarget);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, obstacleCheckRayLength, ObstructionLayerMask))
        {
            return TargetingState.OBSTRUCTED;
        }

        return TargetingState.TARGETING;
    }
}
