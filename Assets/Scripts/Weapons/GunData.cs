﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gun data
/// </summary>
[CreateAssetMenu(fileName = "Create Gun", menuName = "Items/Gun")]
public class GunData : ScriptableObject
{
    [Header("Timing")]
    /// <summary>
    /// Time to equip gun
    /// </summary>
    public float EquipTime = 1.0f;
    /// <summary>
    /// Time to reload gun
    /// </summary>
    public float ReloadTime = 1.0f;
    /// <summary>
    /// Time between shots
    /// </summary>
    [Range(0.01f, 2f)]
    public float FireRate = 0.25f;

    [Header("Properties")]
    /// <summary>
    /// Gun clip size
    /// </summary>
    public int ClipSize = 10;
    /// <summary>
    /// Amount of spread for bullets
    /// </summary>
    [Range(0, 10f)]
    public float SpreadAngle = 0f;
    /// <summary>
    /// Amount that laser reduced bullet spread
    /// </summary>
    [Range(1, 2.5f)]
    public float LaserSpreadReduction = 1.5f;
    /// <summary>
    /// Gun type
    /// </summary>
    public GunType Type;

    [Header("Effects")]
    /// <summary>
    /// Effect when firing gun
    /// </summary>
    public GameObject FireEffect;
    /// <summary>
    /// Gun firing sound
    /// </summary>
    public AudioClip FireSound;
    /// <summary>
    /// Empty click sound
    /// </summary>
    public AudioClip EmptySound;
    /// <summary>
    /// Gun reloading sound
    /// </summary>
    public AudioClip ReloadSound;
}
