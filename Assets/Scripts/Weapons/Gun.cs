﻿using Sirenix.OdinInspector;
using UnityEngine;


/// <summary>
/// Gun state
/// </summary>
public enum GunState
{
    READY,
    FIRING,
    EMPTY,
    RELOADING,
}

/// <summary>
/// Gun type
/// </summary>
public enum GunType
{
    AUTOMATIC,
    PISTOL,
}


[RequireComponent(typeof(AudioSource))]
public class Gun : ExtendedMonoBehaviour
{
    /// <summary>
    /// Gun data
    /// </summary>
    public GunData GunData;
    /// <summary>
    /// Projectile data
    /// </summary>
    public ProjectileData ProjectileData;
    [SerializeField]
    /// <summary>
    /// Number of bullets in clip
    /// </summary>
    private int projectilesInClip;
    /// <summary>
    /// Current gun state
    /// </summary>
    [SerializeField]
    [ReadOnly]
    private GunState state;

    [Header("Miscellaneous")]
    /// <summary>
    /// Prefab used for bullets
    /// </summary>
    [SerializeField]
    private GameObject projectilePrefab;
    /// <summary>
    /// Transform used for firing bullets
    /// </summary>
    public Transform FiringTransform;

    /// <summary>
    /// Whether clip is full
    /// </summary>
    public bool IsClipFull
    {
        get { return projectilesInClip >= GunData.ClipSize; }
    }
    /// <summary>
    /// Whether clip is empty
    /// </summary>
    public bool IsClipEmpty
    {
        get { return projectilesInClip <= 0; }
    }
    /// <summary>
    /// Whether gun is reloading
    /// </summary>
    public bool IsReloading
    {
        get { return state == GunState.RELOADING; }
    }
    /// <summary>
    // Number of projectiles remaining in clip
    /// </summary>
    public int ProjectilesInClip
    {
        get { return projectilesInClip; }
    }

    private AudioSource audioSource;


    void Start()
    {
        if (GunData == null)
        {
            GunData = ScriptableObject.CreateInstance<GunData>();
        }

        audioSource = GetComponent<AudioSource>();

        projectilesInClip = GunData.ClipSize;
        state = GunState.READY;

        // Fallback for missing weapon firing transform
        if (FiringTransform == null)
        {
            FiringTransform = transform;
        }
    }

    void Update()
    {
        // Show direction gun is facing
        if (GameManager.Instance.DebugMode)
        {
            Debug.DrawRay(FiringTransform.position, FiringTransform.forward * 10, Color.blue);
        }
    }


    public void Fire()
    {
        // Cannot fire empty gun
        if (state == GunState.EMPTY) return;
        // Cannot fire while reloading or already firing
        else if (state == GunState.RELOADING || state == GunState.FIRING) return;

        // Weapon spread (pseudo recoil) is affected by whether laser is present
        float spreadAngle = Random.Range(-GunData.SpreadAngle, GunData.SpreadAngle);
        LaserPointer laser = GetComponentInChildren<LaserPointer>(false);
        if (laser != null)
        {
            spreadAngle = spreadAngle / GunData.LaserSpreadReduction;
        }

        Quaternion projectileSpreadRotation = FiringTransform.rotation * Quaternion.Euler(0, spreadAngle, 0);

        GameObject projectile = Instantiate(
            projectilePrefab,
            FiringTransform.position,
            projectileSpreadRotation,
            TemporaryManager.Instance.TemporaryChildren
        );

        // Assign gun's projectile data to new projectile
        projectile.GetComponent<Projectile>().ProjectileData = ProjectileData;

        audioSource.PlayOneShot(GunData.FireSound, 0.1f);

        projectilesInClip--;
        state = GunState.FIRING;

        // TODO: Notify user when attempting to fire empty gun

        if (projectilesInClip > 0)
        {
            Wait(GunData.FireRate, () =>
            {
                state = GunState.READY;
            });
        }
        else
        {
            state = GunState.EMPTY;

            // TODO: Notify user when gun has been emptied

            // "Empty" indicator sound should play shortly after gun is empty
            Wait(0.1f, () =>
            {
                audioSource.PlayOneShot(GunData.EmptySound);
            });
        }
    }

    /// <summary>
    /// Reload the gun
    /// </summary>
    public void Reload()
    {
        // Can only reload while empty or not firing
        if (state != GunState.EMPTY && state != GunState.READY) return;

        audioSource.PlayOneShot(GunData.ReloadSound);

        state = GunState.RELOADING;
        Wait(GunData.ReloadTime, () => {
            projectilesInClip = GunData.ClipSize;
            state = GunState.READY;
        });
    }

    /// <summary>
    /// Toggle the gun's laser (if one exists)
    /// </summary>
    public void ToggleLaser()
    {
        // Note that laser pointer is contained in Gun children (which may be disabled)...
        LaserPointer laser = GetComponentInChildren<LaserPointer>(true);
        laser?.Toggle();
    }
}
