﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

[RequireComponent(typeof(LineRenderer))]
public class LaserPointer : ExtendedMonoBehaviour
{
    /// <summary>
    /// Maximum laser pointer length
    /// </summary>
    [SerializeField]
    private float MaxLength = 10f;
    /// <summary>
    /// Light used for the end dot
    /// </summary>
    [SerializeField]
    private GameObject dot;
    /// <summary>
    /// Layers to ignore for light point collisions
    /// </summary>
    [SerializeField]
    private LayerMask ignoreLayerMask;

    private LineRenderer line;
    private Vector3? laserHitPoint;

    void Start()
    {
        line = GetComponent<LineRenderer>();

        // Laser should start off by default
        Toggle();
    }

    void Update()
    {
        Vector3[] positions = GetLaserPoints();
        line.SetPositions(positions);

        // Laser dot (point light) must be placed "right before" the end point (only if actually hitting object)
        if (laserHitPoint.HasValue)
        {
            dot.transform.position = Vector3Extensions.PointAlongLine(transform.position, laserHitPoint.Value, 0.01f);
            dot.gameObject.SetActive(true);
        }
        else
        {
            dot.gameObject.SetActive(false);
        }
    }


    /// <summary>
    /// Update the laser pointer line renderer endpoints
    /// </summary>
    private Vector3[] GetLaserPoints()
    {
        Vector3 startPosition = transform.position;
        Vector3 endPosition = transform.position + transform.forward * MaxLength;

        // Determine if anything would interfere with laser pointer reaching its maximum length
        Ray ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit, MaxLength, ignoreLayerMask.Invert()))
        {
            laserHitPoint = hit.point;
            endPosition = hit.point;
        }
        else
        {
            laserHitPoint = null;
        }

        Vector3[] positions = new Vector3[]
        {
            startPosition,
            endPosition
        };

        return positions;
    }

    /// <summary>
    /// Toggle the laser pointer
    /// </summary>
    public void Toggle()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
