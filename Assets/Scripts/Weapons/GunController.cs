﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Gun controller state
/// </summary>
public enum GunControllerState
{
    EMPTY,
    EQUIPPING,
    READY,
}


public class GunController : ExtendedMonoBehaviour
{
    [Header("Controls")]
    /// <summary>
    /// Weapon reload key
    /// </summary>
    [SerializeField]
    private KeyCode reloadKey = KeyCode.R;
    /// <summary>
    /// Laser sight key
    /// </summary>
    [SerializeField]
    private KeyCode laserKey = KeyCode.L;

    [Header("Guns")]
    /// <summary>
    /// Position guns are spawned
    /// </summary>
    public Transform GunTransform;
    /// <summary>
    /// Currently equipped gun
    /// </summary>
    [SerializeField]
    [ReadOnly]
    private Gun equippedGun;
    /// <summary>
    /// List of guns available to controller
    /// </summary>
    [SerializeField]
    private List<Gun> guns = new List<Gun>();

    private GunControllerState state = GunControllerState.EMPTY;
    private int equippedGunIdx;

    public Gun EquippedGun
    {
        get { return equippedGun; }
    }
    public bool HasGunEquipped
    {
        get { return equippedGun != null; }
    }


    void Start()
    {
        if (!HasGunEquipped && guns.Count > 0)
        {
            EquipGun(guns[0], true);
            equippedGunIdx = 0;
        }
        else
        {
            state = GunControllerState.EMPTY;
        }
    }

    void Update()
    {
        // Equip a different gun (prevents firing)
        if (Input.GetMouseButtonDown(1) && guns.Count > 0)
        {
            int nextGunIdx = equippedGunIdx >= guns.Count - 1 ? 0 : equippedGunIdx + 1;
            equippedGunIdx = nextGunIdx;

            EquipGun(guns[nextGunIdx]);
        }
        // Other gun actions
        else if (HasGunEquipped && state == GunControllerState.READY)
        {
            // Only reload when clip is not full (prevents firing)
            if (Input.GetKeyDown(reloadKey) && !equippedGun.IsClipFull)
            {
                equippedGun.Reload();
            }
            // Laser can be toggled on and off
            else if (Input.GetKeyDown(laserKey))
            {
                equippedGun.ToggleLaser();
            }
            else if (equippedGun.GunData.Type == GunType.AUTOMATIC)
            {
                if (Input.GetMouseButton(0))
                {
                    equippedGun.Fire();
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    equippedGun.Fire();
                }
            }
        }

        // Weapon should point towards mouse point (within reason)
        if (HasGunEquipped)
        {
            Vector3? mousePoint = CameraUtils.GetMousePointOnPlane(CameraManager.Instance.MainCamera, GunTransform.transform.position.y);
            if (mousePoint.HasValue)
            {
                Vector3 direction = mousePoint.Value.DirectionTo(GunTransform.transform.position);

                // Gun can only point so far towards mouse pointer (prevents unrealistic/rapid rotation when pointer is near gun)
                Quaternion rotation = direction.ClampedRotationTo(GunTransform.transform.forward, 20f);

                equippedGun.transform.rotation = rotation;
            }
        }
    }


    // TODO: Add function to add or remove guns from gun controller (ie. in sync with inventory)


    /// <summary>
    /// Equip a new gun
    /// </summary>
    /// <param name="gun">Gun to equip</param>
    /// <param name="skipEquipTime">Whether equip time is skipped</param>
    public void EquipGun(Gun gun, bool skipEquipTime = false)
    {
        // Can only equip gun when previous gun is ready or empty
        if (state == GunControllerState.EQUIPPING) return;

        if (equippedGun != null)
        {
            // TODO: Switch to pooling (and save clip size)?
            Destroy(equippedGun.gameObject);
            equippedGun = null;
        }

        // Prevent equipping guns the player doesn't have
        bool canEquipGun = guns.Contains(gun);
        if (!canEquipGun) return;

        state = GunControllerState.EQUIPPING;
        float equipDelay = skipEquipTime ? 0 : gun.GunData.EquipTime;

        Wait(equipDelay, () =>
        {
            // TODO: Fix ability to swap weapons to gain full clip
            equippedGun = Instantiate(gun, GunTransform.position, GunTransform.rotation, GunTransform);

            state = GunControllerState.READY;
        });
    }
}
