﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Projectiles deal damage from weapons
/// </summary>
public class Projectile : ExtendedMonoBehaviour
{
    /// <summary>
    /// Projectile data
    /// </summary>
    [ReadOnly]
    public ProjectileData ProjectileData;
    /// <summary>
    /// Layer mask for collisions
    /// </summary>
    public LayerMask CollisionMask;

    private const float MAX_PROJECTILE_DISTANCE = 100f;
    private const float MAX_PROJECTILE_LIFETIME = 10f;
    private Vector3 origin;


    void Start()
    {
        if (ProjectileData == null) ProjectileData = new ProjectileData();

        origin = transform.position;

        // Projectiles can "start" already within a collision
        Collider[] initialCollisions = Physics.OverlapSphere(transform.position, 0.1f, CollisionMask);
        if (initialCollisions.Length > 0)
        {
            OnCollision(initialCollisions[0].gameObject, transform.position);
        }

        // Projectiles have a limited lifetime
        float lifetimeCap = ProjectileData.HasLifetime
            ? Mathf.Min(ProjectileData.MaxLifetime, MAX_PROJECTILE_LIFETIME)
            : MAX_PROJECTILE_LIFETIME;

        Wait(lifetimeCap, () =>
        {
            Destroy(gameObject);
        });
    }

    void Update()
    {
        // Projectiles can have limited range (semi-unusual)
        float distanceCap = ProjectileData.HasMaxRange ? Mathf.Min(ProjectileData.MaxRange, MAX_PROJECTILE_DISTANCE) : MAX_PROJECTILE_DISTANCE;
        float distanceTravelled = Vector3.Distance(origin, transform.position);

        if (distanceTravelled > distanceCap)
        {
            Destroy(gameObject);
            return;
        }

        // Check collisions safely by raycasting (prevents clipping through obstacles).
        //   If a collision will happen within the frame move to within collision distance.
        float frameMoveDistance = ProjectileData.Speed * Time.deltaTime;
        if (CheckCollisions(frameMoveDistance, out float distanceToCollision))
        {
            transform.Translate(Vector3.forward * distanceToCollision);
        }
        else
        {
            transform.Translate(Vector3.forward * frameMoveDistance);
        }

        if (GameManager.Instance.DebugMode)
        {
            Debug.DrawLine(origin, transform.position, Color.blue);
            Debug.DrawRay(transform.position, transform.forward * 10, Color.green);
        }
    }
        

    private void OnCollisionEnter(Collision collider)
    {
        OnCollision(collider.gameObject, collider.GetContact(0).point);
    }
    

    /// <summary>
    /// Check whether a collision will occur within the frame (prevents collision clipping)
    /// </summary>
    /// <param name="frameDistance">Distance that will be moved in frame</param>
    public bool CheckCollisions(float frameDistance, out float hitDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, frameDistance, CollisionMask))
        {
            hitDistance = hit.distance;
            return true;
        }

        hitDistance = 0;
        return false;
    }

    /// <summary>
    /// Handle bullet collision with other entities
    /// </summary>
    /// <param name="collider">Colliding game object</param>
    /// <param name="hitPoint">Location of collision</param>
    private void OnCollision(GameObject collider, Vector3 hitPoint)
    {
        // Only detect collisions on appropriate layers
        if (!CollisionMask.ContainsLayer(collider.layer)) return;

        // Apply damage to collider if appropriate
        Damageable damageableObject = collider.GetComponent<Damageable>();
        if (damageableObject != null)
        {
            damageableObject.TakeDamage(ProjectileData.Damage, gameObject);
        }

        // Sound can be provided by damageable object or default to projectile hit sound
        if (damageableObject != null && damageableObject.DamageSound != null)
        {
            AudioManager.Instance.PlayEffect(damageableObject.DamageSound, transform.position, 0.5f);
        }
        else if (ProjectileData.HitSound != null)
        {
            AudioManager.Instance.PlayEffect(ProjectileData.HitSound, transform.position);
        }

        // Particle effect can be provided by damageable object or default to projectile hit effect
        Quaternion rotation = origin.RotationTo(hitPoint);
        Vector3 effectPoint = Vector3Extensions.PointAlongLine(origin, hitPoint, 0.05f);
        Material colliderMaterial = collider.GetComponent<Renderer>().material;

        if (damageableObject != null && damageableObject.DamageEffect != null) {
            damageableObject.DamageEffect.GetComponent<Renderer>().material = colliderMaterial;
            Instantiate(damageableObject.DamageEffect, effectPoint, rotation, TemporaryManager.Instance.TemporaryChildren);
        }
        else if (ProjectileData.HitEffect != null)
        {
            ProjectileData.HitEffect.GetComponent<Renderer>().material = colliderMaterial;
            Instantiate(ProjectileData.HitEffect, effectPoint, rotation, TemporaryManager.Instance.TemporaryChildren);
        }

        Destroy(gameObject);
    }
}
