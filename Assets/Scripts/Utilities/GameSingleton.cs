﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Taken from: https://gist.github.com/mstevenson/4325117
// Possible idea: https://gist.github.com/michidk/640765fc570220333ac1

/// <summary>
/// Enable singleton pattern for one-off classes
/// </summary>
/// <typeparam name="T">Type of Instance provided by singleton</typeparam>
public class GameSingleton<T> : ExtendedMonoBehaviour where T : Component
{
    private static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(T)) as T;

                if (instance == null)
                {
                    Debug.LogError("An instance of " + typeof(T) + " is needed in the scene but not found");
                }
            }

            return instance;
        }
    }

    // NOTE: This approach causes errors when reloading levels
    //         (references are kept statically but have actually been destroyed).

    /// <summary>
    /// Singleton instance
    /// </summary>
    /*public static T Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = GetComponent<T>();
        }
        else
        {
            Destroy(gameObject);
        }
    }*/

}
