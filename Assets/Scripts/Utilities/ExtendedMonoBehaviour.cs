﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Extended MonoBehaviour utilities
/// </summary>
public class ExtendedMonoBehaviour : MonoBehaviour
{
    /// <summary>
    /// Enable waiting for a timeout before performing a delegate function
    /// </summary>
    /// <param name="seconds">Timeout duration</param>
    /// <param name="action">Delegate callback</param>
    public Coroutine Wait(float seconds, Action action)
    {
        return StartCoroutine(SetTimeout(seconds, action));
    }

    /// <summary>
    /// Perform delegate function after timeout
    /// </summary>
    /// <param name="time">Timeout duration</param>
    /// <param name="callback">Delegate callback</param>
    /// <returns></returns>
    private IEnumerator SetTimeout(float time, Action callback)
    {
        yield return new WaitForSeconds(time);

        callback();
    }

    /// <summary>
    /// Clear a transform's children
    /// </summary>
    /// <param name="objectTransform">Parent game object transform</param>
    protected void ClearChildren(Transform objectTransform)
    {
        int childrenCount = objectTransform.childCount;

        for (int i = childrenCount - 1; i >= 0; i--)
        {
            if (Application.isPlaying)
            {
                Destroy(objectTransform.GetChild(i).gameObject);
            }
            else
            {
                DestroyImmediate(objectTransform.GetChild(i).gameObject);
            }
        }
    }
}
