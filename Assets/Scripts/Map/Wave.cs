﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Wave
{
    /// <summary>
    /// List of wave enemy configurations
    /// </summary>
    public WaveEnemy[] WaveEnemies;
    /// <summary>
    /// Number of enemies in wave
    /// </summary>
    [Range(1, 20)]
    public int EnemySpawnCount = 5;
    /// <summary>
    /// Time between spawning enemies
    /// </summary>
    [Range(0.5f, 5f)]
    public float TimeBetweenSpawns = 2f;
}
