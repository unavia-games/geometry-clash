﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : ExtendedMonoBehaviour
{
    /// <summary>
    /// Map tile coordinates
    /// </summary>
    public Coordinate Coordinates { get => coordinates; }
    /// <summary>
    /// Tile floor game object
    /// </summary>
    public GameObject floorGameObject;

    /// <summary>
    /// Tile type
    /// </summary>
    public TileType Type { get => type; }
    /// <summary>
    /// Whether tile is considered player spawn
    /// </summary>
    public bool IsPlayerSpawn { get => isPlayerSpawn; }
    /// <summary>
    /// Whether tile is spawning an enemy
    /// </summary>
    public bool IsSpawningEnemy { get; private set; }
    /// <summary>
    /// Whether tile is considered open (no obstacles)
    /// </summary>
    public bool Open
    {
        get { return Type == TileType.FLOOR; }
    }

    [ReadOnly]
    [ShowInInspector]
    private Coordinate coordinates;
    [ReadOnly]
    [ShowInInspector]
    private TileType type;
    [ReadOnly]
    [ShowInInspector]
    private bool isPlayerSpawn;

    private Renderer floorRenderer;
    private Material originalFloorMaterial;
    private Material spawnFloorMaterial;
    private Color enemySpawnColor;


    /// <summary>
    /// Coordinates should only be set when spawned by map
    /// </summary>
    /// <param name="coordinates">Tile coordiantes</param>
    public void SetCoordinates(Coordinate coordinates)
    {
        this.coordinates = coordinates;
    }

    /// <summary>
    /// Spawn should only be set once per map
    /// </summary>
    public void SetPlayerSpawn()
    {
        isPlayerSpawn = true;
    }

    /// <summary>
    /// Set the enemy spawn tile color
    /// </summary>
    /// <param name="spawnColor">Enemy spawn tile color</param>
    public void SetEnemySpawnColor(Color spawnColor)
    {
        enemySpawnColor = spawnColor;
    }

    /// <summary>
    /// Set the floor game object
    /// </summary>
    /// <param name="floor"></param>
    public void SetFloorGameObject(GameObject floor)
    {
        floorGameObject = floor;

        // Tiles indicate when enemies are spawning by changing color/material
        floorRenderer = floorGameObject.GetComponent<Renderer>();
        originalFloorMaterial = floorRenderer.sharedMaterial;
        spawnFloorMaterial = new Material(originalFloorMaterial);
        spawnFloorMaterial.color = enemySpawnColor;
    }

    /// <summary>
    /// Highlight that a tile is about to spawn an enemy
    /// </summary>
    /// <param name="timeToSpawn">Whether tile is spawning enemy</param>
    /// <param name="flashSpeed">Tile flashes per second</param>
    public IEnumerator IndicateEnemySpawn(float timeToSpawn, float flashSpeed = 4)
    {
        IsSpawningEnemy = true;
        floorRenderer.material = spawnFloorMaterial;

        float spawnTimer = 0;
        while (spawnTimer < timeToSpawn)
        {
            floorRenderer.material.color = Color.Lerp(originalFloorMaterial.color, enemySpawnColor, Mathf.PingPong(spawnTimer * flashSpeed, 1));

            spawnTimer += Time.deltaTime;
            yield return null;
        }

        IsSpawningEnemy = false;
        floorRenderer.material = originalFloorMaterial;
    }

    /// <summary>
    /// Tile type should only be set when spawned by map
    /// </summary>
    /// <param name="type">Tile type</param>
    public void SetType(TileType type)
    {
        this.type = type;
    }
}
