﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TileType
{
    BORDER,
    FLOOR,
    OBSTACLE
}


/// <summary>
/// Map block types and prefabs
/// </summary>
[Serializable]
public struct TileTypeMap
{
    /// <summary>
    /// Block name
    /// </summary>
    public string Name;
    /// <summary>
    /// Game object/prefab rendered as the tile
    /// </summary>
    public GameObject BlockPrefab;
    /// <summary>
    /// Tile type
    /// </summary>
    public TileType Type;
}


/// <summary>
/// Tile configuration used in map generation tile list
/// </summary>
[Serializable]
public class TileConfig
{
    /// <summary>
    /// Map tile coordinates
    /// </summary>
    public Coordinate Coordinates;
    /// <summary>
    /// Game object/prefab rendered as the tile
    /// </summary>
    public TileTypeMap Block;


    /// <summary>
    /// Whether tile is open
    /// </summary>
    public bool Open
    {
        get { return Block.Type == TileType.FLOOR; }
    }


    public TileConfig(TileTypeMap block, Coordinate coordinates)
    {
        Block = block;
        Coordinates = coordinates;
    }
}
