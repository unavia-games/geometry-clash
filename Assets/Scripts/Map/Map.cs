﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Map
{
    [Header("Configuration")]
    /// <summary>
    /// Map configuration
    /// </summary>
    public MapConfig MapConfig;
    /// <summary>
    /// Enemy waves configuration
    /// </summary>
    public WavesConfig WavesConfig;

    /// <summary>
    /// Map tile components
    /// </summary>
    [HideInInspector]
    public Tile[] Tiles { get; private set; }
    /// <summary>
    /// Map tile configurations
    /// </summary>
    [HideInInspector]
    public TileConfig[,] TileConfigs { get; private set; }
    /// <summary>
    /// Spawn tile game object
    /// </summary>
    [HideInInspector]
    public Tile SpawnTile { get; private set; }

    public Tile[] OpenTiles
    {
        get { return Tiles.ToList().Where(t => t.Type == TileType.FLOOR).ToArray(); }
    }
    public Tile[] ObstacleTiles
    {
        get { return Tiles.ToList().Where(t => t.Type == TileType.OBSTACLE).ToArray(); }
    }


    /// <summary>
    /// Get a tile game object from a position
    /// </summary>
    /// <param name="position">Target position</param>
    /// <returns>Tile game object at specified position</returns>
    public Tile GetTileFromPosition(Vector3 position)
    {
        int x = Mathf.RoundToInt(position.x / MapConfig.TileSize + (MapConfig.MapSize.x - 1) / 2f);
        int y = Mathf.RoundToInt(position.z / MapConfig.TileSize + (MapConfig.MapSize.y - 1) / 2f);

        return GetTileFromCoordinates(new Coordinate(x, y));
    }

    /// <summary>
    /// Get a tile game object from a set of coordinates
    /// </summary>
    /// <param name="coordinates">Target coordinates</param>
    /// <returns>Tile game object with specified coordinates</returns>
    public Tile GetTileFromCoordinates(Coordinate coordinates)
    {
        return Tiles.ToList().Find(t => t.Coordinates == coordinates);
    }

    /// <summary>
    /// Set the map tile configurations
    /// </summary>
    /// <param name="tileConfigs">Map tile configurations</param>
    public void SetTileConfigs(TileConfig[,] tileConfigs)
    {
        // Validate generated map tiles
        if (tileConfigs.GetLength(0) != MapConfig.MapSize.x || tileConfigs.GetLength(1) != MapConfig.MapSize.y)
        {
            throw new Exception("Generated tile configurations do not match map configuration size");
        }

        TileConfigs = tileConfigs;
    }

    /// <summary>
    /// Set the map tile game objects
    /// </summary>
    /// <param name="tiles">Map tile game objects</param>
    public void SetTiles(Tile[] tiles)
    {
        Tiles = tiles;

        // TODO: Handle missing spawn game object
        SpawnTile = tiles.ToList().Find(t => t.IsPlayerSpawn);
    }
}
