﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WaveEnemy
{
    /// <summary>
    /// Enemy prefab
    /// </summary>
    public GameObject EnemyPrefab;
    /// <summary>
    /// Percentage of spawning enemy
    /// </summary>
    [Range(0f, 1f)]
    public float Weight = 1f;
    /// <summary>
    /// Time to spawn enemy
    /// </summary>
    [Range(1f, 5f)]
    public float SpawnTime = 1f;
}
