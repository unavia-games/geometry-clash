﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class MapConfig
{
    [Header("Map")]
    /// <summary>
    /// Map size
    /// </summary>
    public Vector2Int MapSize = new Vector2Int(5, 5);
    /// <summary>
    /// Tile size
    /// </summary>
    [Range(1f, 3f)]
    public float TileSize = 1.5f;
    /// <summary>
    /// Tile percentage occupied by outline
    /// </summary>
    [Range(0.01f, 0.5f)]
    public float TileOutlinePercent = 0.1f;
    /// <summary>
    /// Player spawn
    /// </summary>
    public Coordinate Spawn;
    /// <summary>
    /// Radius around spawn without obstacles
    /// </summary>
    public int SpawnProtection = 1;

    [Header("Obstacles")]
    /// <summary>
    /// Percentage of obstacles in map
    /// </summary>
    [Range(0, 0.8f)]
    public float ObstaclePercent = 0.2f;
    /// <summary>
    /// Random obstacle placement generator seed
    /// </summary>
    [Range(0, 100)]
    public int ObstacleLocationSeed = 10;
    /// <summary>
    /// Obstacle height range
    /// </summary>
    public Vector2 ObstacleHeight = new Vector2(0.75f, 2f);
    /// <summary>
    /// Random obstacle height generator seed
    /// </summary>
    [Range(0, 100)]
    public int ObstacleHeightSeed = 10;

    [Header("Borders")]
    /// <summary>
    /// Border width (number of cells)
    /// </summary>
    [Range(0, 10)]
    public int BorderWidth = 1;
    /// <summary>
    /// Border height range
    /// </summary>
    public Vector2 BorderHeight = new Vector2(1f, 2f);
    /// <summary>
    /// Random border height generator seed
    /// </summary>
    [Range(0, 100)]
    public int BorderHeightSeed = 10;

    [Header("Colors")]
    /// <summary>
    /// Border color
    /// </summary>
    public Color BorderColor;
    /// <summary>
    /// Obstacle color
    /// </summary>
    public Color ObstacleColor;
    /// <summary>
    /// Tile color
    /// </summary>
    public Color TileColor;
    /// <summary>
    /// Enemy spawn tile color
    /// </summary>
    public Color EnemySpawnColor;
}
