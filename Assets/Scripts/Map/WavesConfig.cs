﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WavesConfig
{
    /// <summary>
    /// List of enemy waves
    /// </summary>
    public Wave[] Waves;
    /// <summary>
    /// Time between waves
    /// </summary>
    [Range(1f, 5f)]
    public float TimeBetweenWaves = 2.5f;
    /// <summary>
    /// Randomizer seed for spawning random enemies
    /// </summary>
    [Range(0, 20)]
    public int RandomEnemySeed = 0;
}
