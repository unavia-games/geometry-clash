﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class MapGenerator : ExtendedMonoBehaviour
{
    [Header("Generator Settings")]
    /// <summary>
    /// Map block type mapping (string to prefab)
    /// </summary>
    public TileTypeMap[] Blocks;
    /// <summary>
    /// Tile prefab
    /// </summary>
    public GameObject TilePrefab;

    [Header("Parents")]
    /// <summary>
    /// Tile game objects parent
    /// </summary>
    public Transform TileParent;
    /// <summary>
    /// Tile border game objects parent
    /// </summary>
    public Transform BorderParent;

    [Header("Floor")]
    /// <summary>
    /// Navmesh floor (navigation added on top)
    /// </summary>
    public Transform NavmeshFloor;
    /// <summary>
    /// NavMesh surface for runtime navmesh generation
    /// </summary>
    public NavMeshSurface NavigationSurface;

    // TODO: Encapsualate inside "GenerateTiles" function
    private Queue<TileConfig> shuffledMapTiles;


    /// <summary>
    /// Clear a map's game objects
    /// </summary>
    public void ResetMapGameObjects()
    {
        ClearChildren(TileParent);
        ClearChildren(BorderParent);

        NavmeshFloor.localScale = Vector3.one;
        NavigationSurface.RemoveData();
    }


    /// <summary>
    /// Generate map tiles from a map configuration
    /// </summary>
    /// <param name="mapConfig">Map configuration</param>
    /// <returns>Generated map tiles</returns>
    public TileConfig[,] GenerateTiles(MapConfig mapConfig)
    {
        // NOTE: Borders are not included (not valid playable/map tiles)
        TileConfig[,] mapTiles = new TileConfig[mapConfig.MapSize.x, mapConfig.MapSize.y];
        int numberOfMapTiles = mapTiles.Length;

        ////////////////////////////////////////////////////////////////////////
        // 1. Generate entire map (of tiles)
        for (int x = 0; x < mapConfig.MapSize.x; x++)
        {
            for (int y = 0; y < mapConfig.MapSize.y; y++)
            {
                Coordinate coordinates = new Coordinate(x, y);
                TileTypeMap tileBlock = GetBlock("floor");

                TileConfig newTile = new TileConfig(tileBlock, coordinates);
                mapTiles[x, y] = newTile;
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // 2. Generate obstacles
        TileConfig[] baseShuffledMapTiles = CollapseTiles(mapTiles).Shuffle(mapConfig.ObstacleLocationSeed);
        TileConfig[] availableShuffledMapTiles = baseShuffledMapTiles.Where(t => !IsTileBySpawn(mapConfig.Spawn, t.Coordinates, mapConfig.SpawnProtection)).ToArray();
        shuffledMapTiles = new Queue<TileConfig>(availableShuffledMapTiles);
        int availableObstacleTilesCount = shuffledMapTiles.Count;

        // Debug.Log("Available Obstacle Tiles: " + availableObstacleTilesCount);

        System.Random obstacleHeightGenerator = new System.Random(mapConfig.ObstacleHeightSeed);
        int maxObstacleCount = (int)Math.Floor(availableObstacleTilesCount * mapConfig.ObstaclePercent);
        int obstacleCount = 0;

        // Debug.Log("Expected Obstacle Tiles: " + maxObstacleCount);

        for (int i = 0; i < maxObstacleCount; i++)
        {
            // Map cannot have more obstacles than available tiles
            if (i - 1 > availableObstacleTilesCount) break;

            // NOTE: Obstacles must be set BEFORE checking whether the map is STILL accessible
            TileConfig random = GetRandomMapTile();

            random.Block = GetBlock("obstacle");
            obstacleCount++;

            // Obstacles will only be spawned if the entire map will be still accessible
            //   and spawn is empty (undos changes necessary for fill check).
            if (mapConfig.Spawn == random.Coordinates || !IsMapFullyAccessible(mapTiles, mapConfig, obstacleCount))
            {
                random.Block = GetBlock("floor");
                obstacleCount--;
                continue;
            }
        }

        // Debug.Log("Obstacles Tiles: " + obstacleCount);

        return mapTiles;
    }

    /// <summary>
    /// Draw a map tiles and borders
    /// </summary>
    /// <param name="mapConfig">Map configuration</param>
    /// <returns></returns>
    public Tile[] DrawTiles(MapConfig mapConfig, TileConfig[,] mapTileConfigs)
    {
        Tile[] tileGameObjects = new Tile[mapTileConfigs.Length];

        // Clear previously drawn tiles (to avoid cluttering hierarchy)
        ResetMapGameObjects();

        int tileCounter = 0;
        System.Random obstacleHeightGenerator = new System.Random(mapConfig.ObstacleHeightSeed);

        for (int x = 0; x < mapConfig.MapSize.x; x++)
        {
            for (int y = 0; y < mapConfig.MapSize.y; y++)
            {
                ////////////////////////////////////////////////////////////////////////
                // 1. Create parent tiles
                TileConfig tileConfig = mapTileConfigs[x, y];
                Vector3 position = GetPositionFromCoordinate(mapConfig, x, y);
                bool isSpawn = tileConfig.Coordinates == mapConfig.Spawn;

                // Create and configure the tile components
                GameObject tileGameObject = Instantiate(TilePrefab, position, Quaternion.identity, TileParent);
                Tile tileComponent = tileGameObject.GetComponent<Tile>();

                tileComponent.SetCoordinates(tileConfig.Coordinates);
                tileComponent.SetType(tileConfig.Block.Type);
                tileComponent.SetEnemySpawnColor(mapConfig.EnemySpawnColor);
                if (isSpawn)
                {
                    tileComponent.SetPlayerSpawn();
                }

                tileGameObjects[tileCounter++] = tileComponent;

                ////////////////////////////////////////////////////////////////////////
                // 2. Draw map tile floors
                TileTypeMap floor = GetBlock("floor");
                GameObject floorPrefab = floor.BlockPrefab;
                GameObject floorGameObject = Instantiate(floorPrefab, position, Quaternion.LookRotation(Vector3.down), tileGameObject.transform);

                tileComponent.SetFloorGameObject(floorGameObject);

                // Tile borders are faked by scaling tiles down
                floorGameObject.transform.localScale = Vector3.one * (1 - mapConfig.TileOutlinePercent) * mapConfig.TileSize;

                Renderer tileRenderer = floorGameObject.GetComponent<Renderer>();
                Material tileMaterial = tileRenderer.sharedMaterial;
                tileMaterial.color = mapConfig.TileColor;

                // Spawn is indicated by a colorful square
                if (isSpawn)
                {
                    Renderer spawnRenderer = floorGameObject.GetComponent<Renderer>();
                    Material spawnMaterial = new Material(spawnRenderer.sharedMaterial);
                    Color spawnColor = mapConfig.BorderColor;
                    spawnMaterial.color = spawnColor;
                    spawnRenderer.material = spawnMaterial;
                }

                ////////////////////////////////////////////////////////////////////////
                // 3. Draw map obstacles
                if (tileConfig.Block.Type == TileType.OBSTACLE)
                {
                    Vector3 obstacleScale = new Vector3(
                        mapConfig.TileSize,
                        (float)obstacleHeightGenerator.NextDouble() * (mapConfig.ObstacleHeight.y - mapConfig.ObstacleHeight.x) + mapConfig.ObstacleHeight.x,
                        mapConfig.TileSize
                    );

                    TileTypeMap obstacle = GetBlock("obstacle");
                    GameObject obstaclePrefab = obstacle.BlockPrefab;
                    GameObject obstacleGameObject = Instantiate(obstaclePrefab, position + (Vector3.up * obstacleScale.y / 2f), Quaternion.identity, tileGameObject.transform);

                    obstacleGameObject.transform.localScale = obstacleScale;

                    Renderer obstacleRenderer = obstacleGameObject.GetComponent<Renderer>();
                    Material obstacleMaterial = obstacleRenderer.sharedMaterial;
                    obstacleMaterial.color = mapConfig.ObstacleColor;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // 4. Draw map borders (not included in tiles)
        if (mapConfig.BorderWidth > 0)
        {
            System.Random borderHeightGenerator = new System.Random(mapConfig.BorderHeightSeed);

            int borderStartX = 0 - mapConfig.BorderWidth;
            int borderStartY = 0 - mapConfig.BorderWidth;
            int borderEndX = mapConfig.MapSize.x + mapConfig.BorderWidth;
            int borderEndY = mapConfig.MapSize.y + mapConfig.BorderWidth;

            for (int x = borderStartX; x < borderEndX; x++)
            {
                for (int y = borderStartY; y < borderEndY; y++)
                {
                    // Only border cells should be rendered (based on width outside of map)
                    bool isInBorderX = (x >= borderStartX && x < 0) || (x < borderEndX && x > mapConfig.MapSize.x - 1);
                    bool isInBorderY = (y >= borderStartY && y < 0) || (y < borderEndY && y > mapConfig.MapSize.y - 1);

                    if (!(isInBorderX || isInBorderY)) continue;

                    Vector3 borderPosition = GetPositionFromCoordinate(mapConfig, x, y);
                    Vector3 borderScale = new Vector3(
                        mapConfig.TileSize,
                        (float)borderHeightGenerator.NextDouble() * (mapConfig.BorderHeight.y - mapConfig.BorderHeight.x) + mapConfig.TileSize,
                        mapConfig.TileSize
                    );

                    TileTypeMap border = GetBlock("border");
                    GameObject borderPrefab = border.BlockPrefab;
                    GameObject borderGameObject = Instantiate(borderPrefab, borderPosition + (Vector3.up * borderScale.y / 2f), Quaternion.identity, BorderParent);
                    borderGameObject.transform.localScale = borderScale;

                    Renderer borderRenderer = borderGameObject.GetComponent<Renderer>();
                    Material borderMaterial = borderRenderer.sharedMaterial;
                    borderMaterial.color = mapConfig.BorderColor;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////
        // 5. Map navigation
        NavmeshFloor.localScale = new Vector3(mapConfig.MapSize.x * mapConfig.TileSize, mapConfig.MapSize.y * mapConfig.TileSize, 1);
        NavigationSurface.BuildNavMesh();

        return tileGameObjects;
    }




    /// <summary>
    /// Collapse a 2D array into a 1D array
    /// </summary>
    /// <param name="tiles">2 dimension array of tiles</param>
    /// <returns>Single dimension array of tiles</returns>
    public TileConfig[] CollapseTiles(TileConfig[,] tiles)
    {
        TileConfig[] collapsedTiles = new TileConfig[tiles.Length];
        int counter = 0;

        for (int x = 0; x < tiles.GetLength(0); x++)
        {
            for (int y = 0; y < tiles.GetLength(1); y++)
            {
                collapsedTiles[counter] = tiles[x, y];
                counter++;
            }
        }

        return collapsedTiles;
    }

    /// <summary>
    /// Get a block from the dictionary
    /// </summary>
    /// <param name="name">Block search name</param>
    /// <returns>Matching block</returns>
    public TileTypeMap GetBlock(string name)
    {
        bool isValidBlock = Blocks.ToList().Any(b => b.Name == name);

        if (!isValidBlock) throw new Exception("No matching block type found for " + name);

        return Blocks.ToList().Find(b => b.Name == name);
    }

    /// <summary>
    /// Get the world position for a map coordinate.
    /// </summary>
    /// <returns>World position from map coordinate.</returns>
    /// <param name="x">Map X coordinate.</param>
    /// <param name="y">Map Y coordinate.</param>
    private Vector3 GetPositionFromCoordinate(MapConfig mapConfig, int x, int y)
    {
        return new Vector3(
            x * mapConfig.TileSize - (mapConfig.MapSize.x * mapConfig.TileSize / 2f) + (mapConfig.TileSize / 2f),
            0,
            y * mapConfig.TileSize - (mapConfig.MapSize.y * mapConfig.TileSize / 2f) + (mapConfig.TileSize / 2f)
        );
    }

    /// <summary>
    /// Get a random map tile.
    /// </summary>
    /// <returns>Random tile.</returns>
    private TileConfig GetRandomMapTile()
    {
        TileConfig random = shuffledMapTiles.Dequeue();
        shuffledMapTiles.Enqueue(random);

        return random;
    }

    /// <summary>
    /// Determines whether the map is still fully accessible (flood fill algorithm)
    /// </summary>
    /// <returns>Whether map is fully accessible</returns>
    /// <param name="tiles">List of tiles</param>
    /// <param name="mapConfig">Map configuration</param>
    /// <param name="currentObstacleCount">Current obstacle count</param>
    private bool IsMapFullyAccessible(TileConfig[,] tiles, MapConfig mapConfig, int currentObstacleCount)
    {
        bool[,] checkedTiles = new bool[tiles.GetLength(0), tiles.GetLength(1)];
        Queue<TileConfig> uncheckedTiles = new Queue<TileConfig>();

        // Spawn is already known to be accessible
        TileConfig spawnTile = CollapseTiles(tiles).ToList().Find(t => t.Coordinates == mapConfig.Spawn);
        if (spawnTile == null)
        {
            // TODO: Handle missing spawn
            Debug.LogError("Missing spawn: " + mapConfig.Spawn);
        }

        uncheckedTiles.Enqueue(spawnTile);
        checkedTiles[mapConfig.Spawn.X, mapConfig.Spawn.Y] = true;
        int accessibleTileCount = 1;

        while (uncheckedTiles.Count > 0)
        {
            TileConfig tile = uncheckedTiles.Dequeue();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    int neighbourX = tile.Coordinates.X + x;
                    int neighbourY = tile.Coordinates.Y + y;

                    // Only check horizontally and vertically adjacent tiles
                    if (x != 0 && y != 0) continue;

                    // Only check neighboring tiles that are within the map boundaries
                    if (!(neighbourX >= 0 && neighbourX < tiles.GetLength(0) && neighbourY >= 0 && neighbourY < tiles.GetLength(1))) continue;

                    // Skip tiles that have been checked or are obstacles
                    if (checkedTiles[neighbourX, neighbourY] || !tiles[neighbourX, neighbourY].Open) continue;

                    checkedTiles[neighbourX, neighbourY] = true;
                    TileConfig uncheckedTile = CollapseTiles(tiles).ToList().Find(t => t.Coordinates.X == neighbourX && t.Coordinates.Y == neighbourY);
                    if (uncheckedTile == null)
                    {
                        // TODO: Handle missing neighbour tile
                        Debug.LogError("Missing neighbour tile: " + uncheckedTile.Coordinates);
                    }

                    uncheckedTiles.Enqueue(uncheckedTile);
                    accessibleTileCount++;
                }
            }
        }

        // Mismatching accessible tile counts means that the last-placed tile broke the accessibility
        int targetAccessibleTileCount = mapConfig.MapSize.x * mapConfig.MapSize.y - currentObstacleCount;

        return targetAccessibleTileCount == accessibleTileCount;
    }

    /// <summary>
    /// Determine whether a tile is a neighbour to spawn
    /// </summary>
    /// <param name="spawn">Spawn position</param>
    /// <param name="tile">Tile position</param>
    /// <param name="distance">Distance to check from spawn</param>
    /// <returns>Whether tile is a neighbour to spawn</returns>
    private bool IsTileBySpawn(Coordinate spawn, Coordinate tile, int distance = 1)
    {
        if (spawn == tile) return true;

        return Math.Abs(spawn.X - tile.X) <= distance && Math.Abs(spawn.Y - tile.Y) <= distance;
    }
}
