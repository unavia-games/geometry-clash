﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawner : ExtendedMonoBehaviour
{
    /// <summary>
    /// Parent for spawned children
    /// </summary>
    public Transform SpawnedChildren;

    [Header("Anti-Camping")]
    /// <summary>
    /// Time between camping checks
    /// </summary>
    [Range(0.5f, 5f)]
    public float timeBetweenCampingChecks = 2.0f;
    /// <summary>
    /// Distance player must move to avoid camping penalty
    /// </summary>
    [Range(0.5f, 2.5f)]
    public float campThresholdDistance = 1.5f;

    [Header("Events")]
    /// <summary>
    /// Callback after all waves have been defeated
    /// </summary>
    public Action<Map> MapEndCallback;
    /// <summary>
    /// Callback after a wave has been started
    /// </summary>
    public Action<Wave, int> WaveStartCallback;
    /// <summary>
    /// Callback after a wave has been defeated
    /// </summary>
    public Action<Wave, int> WaveEndCallback;

    private Wave currentWave;
    private Map currentMap;

    private Coroutine spawnCoroutine;
    private System.Random enemyRandomizer;
    private int enemiesRemainingInWave;
    private int enemiesRemainingToSpawn;
    private int waveNumber;

    private Coroutine campingCheckCoroutine;
    private Vector3 oldCampPosition;
    private bool isCamping = false;


    private void Start()
    {
        if (GameManager.Instance?.PlayerGameObject)
        {
            GameManager.Instance.PlayerGameObject.GetComponent<Damageable>().OnDeath += OnPlayerDeath;
        }
    }

    private void OnDestroy()
    {
        if (GameManager.Instance?.PlayerGameObject)
        {
            GameManager.Instance.PlayerGameObject.GetComponent<Damageable>().OnDeath -= OnPlayerDeath;
        }
    }


    /// <summary>
    /// Start the enemy spawner
    /// </summary>
    /// <param name="map">Current map</param>
    public void StartForMap(Map map)
    {
        waveNumber = 0;
        currentMap = map;

        enemyRandomizer = new System.Random(map.WavesConfig.RandomEnemySeed);

        campingCheckCoroutine = StartCoroutine(CheckForCamping());

        NextWave();
    }

    /// <summary>
    /// Cleanup the enemy spawner
    /// </summary>
    public void StopForMap()
    {
        StopCoroutine(spawnCoroutine);

        ClearChildren(SpawnedChildren);
    }

    /// <summary>
    /// Prepare next wave of enemies
    /// </summary>
    private void NextWave()
    {
        // Prevent exceeding number of waves
        if (++waveNumber > currentMap.WavesConfig.Waves.Length)
        {
            MapEndCallback?.Invoke(currentMap);
            return;
        }

        // Only start waves while player is alive
        if (GameManager.Instance.PlayerGameObject == null) return;

        currentWave = currentMap.WavesConfig.Waves[waveNumber - 1];

        enemiesRemainingToSpawn = currentWave.EnemySpawnCount;
        enemiesRemainingInWave = enemiesRemainingToSpawn;

        // Start next wave (after timeout)
        Wait(currentMap.WavesConfig.TimeBetweenWaves, () =>
        {
            WaveStartCallback?.Invoke(currentWave, waveNumber);

            spawnCoroutine = StartCoroutine(SpawnEnemy());
        });
    }

    /// <summary>
    /// Determine whether the player is camping
    /// </summary>
    /// <returns>Whether the player is camping</returns>
    private IEnumerator CheckForCamping()
    {
        while (GameManager.Instance.PlayerGameObject != null)
        {
            Vector3 currentCampingPosition = GameManager.Instance.PlayerGameObject.transform.position;
            isCamping = Vector3.Distance(oldCampPosition, currentCampingPosition) < campThresholdDistance;
            oldCampPosition = currentCampingPosition;

            yield return new WaitForSeconds(timeBetweenCampingChecks);
        }
    }

    /// <summary>
    /// Spawn enemies while a wave lasts
    /// </summary>
    private IEnumerator SpawnEnemy()
    {
        while (enemiesRemainingToSpawn > 0)
        {
            enemiesRemainingToSpawn--;

            // Enemies are chosen randomly using a weighted (seeded) system
            WaveEnemy randomEnemy = currentWave.WaveEnemies.ToList().RandomByWeight(e => e.Weight, enemyRandomizer);

            // Spawn enemies at player position if they are "camping"
            Tile spawnTile = isCamping
                ? currentMap.GetTileFromPosition(oldCampPosition)
                : currentMap.Tiles.ToList().Where(t => t.Type == TileType.FLOOR && !t.IsSpawningEnemy && !t.IsPlayerSpawn).Random();

            StartCoroutine(spawnTile.IndicateEnemySpawn(randomEnemy.SpawnTime));

            Wait(randomEnemy.SpawnTime, () => {
                if (GameManager.Instance.PlayerGameObject != null)
                {
                    Quaternion rotationToPlayer = GameManager.Instance.PlayerGameObject.transform.position.RotationTo(spawnTile.transform.position);
                    GameObject spawnedEnemy = Instantiate(randomEnemy.EnemyPrefab, spawnTile.transform.position + Vector3.up, rotationToPlayer, SpawnedChildren);
                    Enemy spawnedEnemyComponent = spawnedEnemy.GetComponent<Enemy>();

                    // Subscribe to death event
                    spawnedEnemyComponent.Damageable.OnDeath += OnEnemyDeath;
                }
            });

            yield return new WaitForSeconds(currentWave.TimeBetweenSpawns);
        }

        StopCoroutine(spawnCoroutine);
    }


    ////////////////////////////////////////////////////////////////////////////
    // Events

    /// <summary>
    /// Handle player death (stop spawning)
    /// </summary>
    /// <param name="killer">Object that killed player</param>
    private void OnPlayerDeath(GameObject killer)
    {
        StopCoroutine(spawnCoroutine);
    }

    /// <summary>
    /// Respond to enemy death
    /// <param name="damager">Object inflicting damage</param>
    /// </summary>
    private void OnEnemyDeath(GameObject damager)
    {
        if (--enemiesRemainingInWave > 0) return;

        // Must stop current spawning coroutine
        StopCoroutine(spawnCoroutine);

        WaveEndCallback?.Invoke(currentWave, waveNumber);

        // Next wave starts immediately after last enemy death (any delay is specifed in "NextWave")
        NextWave();
    }
}
