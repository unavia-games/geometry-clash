﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        GameManager gameManager = target as GameManager;
        MapGenerator generator = gameManager.MapGenerator;

        if (DrawDefaultInspector())
        {
            gameManager.CreateMap();
        }

        ////////////////////////////////////////////////////////////////////////
        // Map pagination

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Draw Map"))
        {
            gameManager.CreateMap();
        }

        if (GUILayout.Button("Reset Map"))
        {
            generator.ResetMapGameObjects();
        }

        GUILayout.EndHorizontal();

        ////////////////////////////////////////////////////////////////////////
        // Map pagination

        /*GUILayout.BeginHorizontal();

        // TODO: End spawning and clear enemies when changing maps (possibly limit to Edit mode).

        if (GUILayout.Button("Next Map"))
        {
            gameManager.MoveToNextMap();
            gameManager.CreateCurrentMap();
        }

        if (GUILayout.Button("Previous Map"))
        {
            gameManager.MoveToPreviousMap();
            gameManager.CreateCurrentMap();
        }
        
        GUILayout.EndHorizontal();*/
    }
}
