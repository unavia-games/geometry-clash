﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        MapGenerator generator = target as MapGenerator;

        if (GUILayout.Button("Clear Map"))
        {
            generator.ResetMapGameObjects();
        }
    }
}
