﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Target;
    public float Smoothing = 5f;

    private Vector3 offset;

    private void Start()
    {
        // Initial camera offset
        offset = transform.position - Target.position;
    }

    /// <summary>
    /// Follow target in FixedUpdate (in case target also moves in FixedUpdate)
    /// </summary>
    void FixedUpdate()
    {
        Vector3 targetCameraPosition = Target.position + offset;

        // Smoothly interpolate between the camera's current position and it's target position
        transform.position = Vector3.Lerp(transform.position, targetCameraPosition, Smoothing * Time.deltaTime);
    }
}
