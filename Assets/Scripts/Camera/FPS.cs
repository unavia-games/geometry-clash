﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FPS : MonoBehaviour
{
    public TextMeshProUGUI FpsText;
    public float UpdateRate = 4f;

    private float deltaTime = 0f;
    private float frameCount = 0f;
    private float fps = 0f;


    void Update()
    {
        CalculateFps();
    }


    private void CalculateFps()
    {
        frameCount++;
        deltaTime += Time.deltaTime;

        if (deltaTime <= 1f / UpdateRate) return;

        // FPS is only calculated and updated when update rate is reached
        fps = frameCount / deltaTime;
        frameCount = 0f;
        deltaTime -= 1f / UpdateRate;

        FpsText.text = "FPS: " + Mathf.Ceil(fps).ToString();

        // Use color to indicate general FPS
        FpsText.color = Color.white;

        if (fps < 120)
        {
            FpsText.color = Color.green;
        }
        else if (fps < 60)
        {
            FpsText.color = Color.yellow;
        }
        else if (fps < 30)
        {
            FpsText.color = Color.red;
        }
    }
}
