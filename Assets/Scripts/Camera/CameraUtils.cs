﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CameraUtils : MonoBehaviour
{
    /// <summary>
    /// Camera reference for utility functions.
    /// </summary>
    public new Camera camera;
    /// <summary>
    /// Whether mouse intersection position is drawn.
    /// </summary>
    public bool ShowMousePosition = true;
    /// <summary>
    /// Mouse intersection position display.
    /// </summary>
    public TextMeshProUGUI MousePositionText;
    /// <summary>
    /// Whether mouse cursor is locked while running.
    /// </summary>
    public bool LockCursor = false;

    private Vector3? mouseIntersection;
    private Vector3? mousePlanePoint;

    void Awake()
    {
        camera = GetComponent<Camera>();

        // Optionally lock cursor while running
        if (LockCursor)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    void Update()
    {
        // Mouse intersection position indicator
        if (ShowMousePosition)
        {
            mouseIntersection = GetMousePointInWorld(camera);
            MousePositionText.text = mouseIntersection.HasValue ? mouseIntersection.ToString() : "No Intersect";
        }
    }


    /// <summary>
    /// Calculate where the mouse intersects with a collider
    /// </summary>
    /// <param name="camera">Camera reference.</param>
    /// <returns>The mouse intersection position.</returns>
    public static Vector3? GetMousePointInWorld(Camera camera)
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        // Ignore the UI layer when finding mouse intersection
        LayerMask ignoreLayerMask = ~(1 << LayerMask.NameToLayer("UI"));

        if (Physics.Raycast(ray, out RaycastHit hit, float.MaxValue, ignoreLayerMask)) {
            return hit.point;
        }

        return null;
    }

    /// <summary>
    /// Calculate where the mouse intersects with a plane
    /// </summary>
    /// <param name="camera">Camera reference.</param>
    /// <param name="planeY">Y level of plane.</param>
    /// <returns>The mouse intersection position.</returns>
    public static Vector3? GetMousePointOnPlane(Camera camera, float planeY = 0)
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        // QUESTION: Why does this need to be "down" normal? If using "up" the intersection will have an invalid "y" value (flipped negative)?
        // QUESTION: Why does this interfere with the other "mouseIntersection"?
        Plane plane = new Plane(Vector3.down, planeY);

        if (plane.Raycast(ray, out float distance))
        {
            return ray.GetPoint(distance);
        }

        return null;
    }


    private void OnDrawGizmos()
    {
        // Mouse intersection position indicator
        if (ShowMousePosition && mouseIntersection.HasValue)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(mouseIntersection.Value, 0.1f);
            Gizmos.DrawLine(transform.position, mouseIntersection.Value);
        }
    }
}
