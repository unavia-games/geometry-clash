﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Wanderer : ExtendedMonoBehaviour
{
    /// <summary>
    /// Random refresh rate that wanderer use to find new target location
    /// </summary>
    public Vector2 WanderPointRefreshRate = new Vector2(2, 10);
    /// <summary>
    /// Range wanderer will search for a new target location
    /// </summary>
    [Range(1, 20)]
    public float WanderRadius = 5;

    private NavMeshAgent pathfinder;


    private void Awake()
    {
        pathfinder = GetComponent<NavMeshAgent>();
    }


    /// <summary>
    /// Randomly wander as long as coroutine runs
    /// </summary>
    /// <returns>Coroutine</returns>
    public IEnumerator Wander()
    {
        while (true)
        {
            if (!pathfinder.enabled) yield return null;

            Vector3 newDestination = RandomPointInNavSphere(transform.position, WanderRadius);
            pathfinder.SetDestination(newDestination);

            float randomWaitTime = Random.Range(WanderPointRefreshRate.x, WanderPointRefreshRate.y);
            yield return new WaitForSeconds(randomWaitTime);
        }
    }

    /// <summary>
    /// Find a random accessible navigation point
    /// </summary>
    /// <param name="origin">Wander agent origin</param>
    /// <param name="radius">Wander radius</param>
    /// <param name="layerMask">Wander navigable layer mask</param>
    /// <returns>Randomized navigation point</returns>
    public static Vector3 RandomPointInNavSphere(Vector3 origin, float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += origin;

        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, radius, 1);

        return hit.position;
    }
}
