﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Damageable))]
public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    public float MoveSpeed = 5;

    private Rigidbody rb;
    private Vector3 velocity;
    private Quaternion rotation = Quaternion.identity;


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        GetMovementInput();
        GetRotationInput();
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);

        rb.MoveRotation(rotation);
    }


    /// <summary>
    /// Get the player's movement input
    /// </summary>
    void GetMovementInput()
    {
        // Get input and determine velocity (after normalizing to get direction only)
        Vector3 moveInput = new Vector3(
            Input.GetAxisRaw("Horizontal"),
            0,
            Input.GetAxisRaw("Vertical")
        );
        velocity = moveInput.normalized * MoveSpeed;
    }

    void GetRotationInput()
    {
        // Only rotate when target is a valid point
        Vector3? facePoint = CameraUtils.GetMousePointOnPlane(CameraManager.Instance.MainCamera, transform.position.y);
        if (!facePoint.HasValue) return;

        // Zero out the "y" rotation to remove unwanted rotation in that axis
        Vector3 direction = (facePoint.Value - transform.position).normalized;
        direction.y = 0;
        rotation = Quaternion.LookRotation(direction);
    }
}
