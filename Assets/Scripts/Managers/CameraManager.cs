﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class CameraManager : GameSingleton<CameraManager>
{
    /// <summary>
    /// Main game camera.
    /// </summary>
    [Required]
    public Camera MainCamera;
}
