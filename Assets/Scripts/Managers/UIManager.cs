﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum FadeDirection
{
    HIDE,
    SHOW
}


public class UIManager : GameSingleton<UIManager>
{
    /// <summary>
    /// Fade background image
    /// </summary>
    public Image FadeBackgroundImage;
    /// <summary>
    /// Game over UI canvas
    /// </summary>
    public Canvas GameOverUI;


    private void Start()
    {
        if (GameManager.Instance.PlayerGameObject != null)
        {
            GameManager.Instance.PlayerGameObject.GetComponent<Damageable>().OnDeath += OnGameOver;
        }
    }

    private void OnDestroy()
    {
        if (GameManager.Instance.PlayerGameObject != null)
        {
            GameManager.Instance.PlayerGameObject.GetComponent<Damageable>().OnDeath -= OnGameOver;
        }
    }


    /// <summary>
    /// Fade the background over time
    /// </summary>
    /// <param name="direction">Fade direction</param>
    /// <param name="fadeTime">Fade time</param>
    private IEnumerator FadeBackground(FadeDirection direction, float fadeTime = 0)
    {
        float speed = 1 / fadeTime;
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            FadeBackgroundImage.color = direction == FadeDirection.SHOW
                ? Color.Lerp(Color.clear, Color.black, percent)
                : Color.Lerp(Color.black, Color.clear, percent);
            yield return null;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Events

    /// <summary>
    /// Show game over UI
    /// </summary>
    /// <param name="killer">Entity that killed the player</param>
    private void OnGameOver(GameObject killer)
    {
        float backgroundFadeTime = 1f;

        StartCoroutine(FadeBackground(FadeDirection.SHOW, backgroundFadeTime));

        Wait(backgroundFadeTime, () =>
        {
            GameOverUI.gameObject.SetActive(true);
        });
    }

    /// <summary>
    /// Prepare UI when starting game
    /// </summary>
    private void OnGameStart()
    {
        StartCoroutine(FadeBackground(FadeDirection.HIDE));

        GameOverUI.gameObject.SetActive(false);
    }
}
