﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : GameSingleton<GameManager>
{
    // Whether game is in debug mode
    public bool DebugMode = true;

    [Header("Game Objects")]
    /// <summary>
    /// Player game object
    /// </summary>
    [Required]
    public GameObject PlayerGameObject;
    /// <summary>
    /// Map generator
    /// </summary>
    [Required]
    public MapGenerator MapGenerator;
    /// <summary>
    /// Enemy spawner
    /// </summary>
    [Required]
    public EnemySpawner EnemySpawner;

    [Header("Maps")]
    /// <summary>
    /// List of game maps
    /// </summary>
    public Map[] Maps;
    /// <summary>
    /// Current map index
    /// </summary>
    public int CurrentMapIdx = 0;

    [ShowInInspector]
    [ReadOnly]
    private Map currentMap;


    private void Start()
    {
        EnemySpawner.MapEndCallback = OnMapEnd;
        EnemySpawner.WaveEndCallback = OnWaveEnd;
        EnemySpawner.WaveStartCallback = OnWaveStart;

        // Player is disabled until map has been generated
        PlayerGameObject.SetActive(false);

        // Generate and draw initial map
        GetCurrentMap();
        CreateMap();
        StartMap();

        PlayerGameObject.SetActive(true);
    }


    /// <summary>
    /// Get the current map (based on map index)
    /// </summary>
    private void GetCurrentMap()
    {
        if (Maps.Length == 0) throw new Exception("No maps specified");
        if (CurrentMapIdx < 0 || CurrentMapIdx >= Maps.Length) throw new Exception("Invalid map index specified");

        Map foundMap = Maps[CurrentMapIdx];
        currentMap = foundMap ?? throw new Exception("Map not found for index " + CurrentMapIdx);
    }

    /// <summary>
    /// Create and draw a map
    /// </summary>
    public void CreateMap()
    {
        GetCurrentMap();

        TileConfig[,] mapTileConfigs = MapGenerator.GenerateTiles(currentMap.MapConfig);
        currentMap.SetTileConfigs(mapTileConfigs);

        Tile[] mapTiles = MapGenerator.DrawTiles(currentMap.MapConfig, currentMap.TileConfigs);
        currentMap.SetTiles(mapTiles);

        // TODO: "Animate" player spawn transition

        // Move the player to spawn
        PlayerGameObject.transform.position = currentMap.SpawnTile.transform.position + Vector3.up;
    }

    /// <summary>
    /// Restart the game (or restart)
    /// </summary>
    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }

    /// <summary>
    /// Start the current map (enemy spawning, etc)
    /// </summary>
    public void StartMap()
    {
        EnemySpawner.StartForMap(currentMap);
    }

    /// <summary>
    /// Cleanup the current map (enemy spawning, enemies, etc)
    /// </summary>
    public void StopMap()
    {
        EnemySpawner.StopForMap();
    }

    /// <summary>
    /// Move to the previous map
    /// </summary>
    public void MoveToPreviousMap()
    {
        if (CurrentMapIdx <= 0) return;

        // End spawning, clear enemies, etc
        StopMap();

        CurrentMapIdx--;
        GetCurrentMap();
    }

    /// <summary>
    /// Move to the next map
    /// </summary>
    public void MoveToNextMap()
    {
        if (CurrentMapIdx >= Maps.Length - 1) return;

        // End spawning, clear enemies, etc
        StopMap();

        CurrentMapIdx++;
        GetCurrentMap();
    }


    private void OnMapEnd(Map map)
    {
        Debug.Log("Map finished");

        if (CurrentMapIdx < Maps.Length - 1)
        {
            MoveToNextMap();
            CreateMap();
            StartMap();
        }
    }

    private void OnWaveEnd(Wave wave, int waveNumber)
    {
        Debug.Log("Wave ended");
    }

    private void OnWaveStart(Wave wave, int waveNumber)
    {
        Debug.Log("Wave started");
    }
}
