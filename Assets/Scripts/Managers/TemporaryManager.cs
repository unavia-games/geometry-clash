﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provide utilities for temporary objects
/// </summary>
public class TemporaryManager : GameSingleton<TemporaryManager>
{
    /// <summary>
    /// Transform for temporary children to use as parent
    /// </summary>
    [ReadOnly]
    public Transform TemporaryChildren;


    private void Start()
    {
        TemporaryChildren = transform;
    }
}
